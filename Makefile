.PHONY: all argocd

info:
	@echo "Available commands:"
	@echo ""
	@echo "Command                              Description"
	@echo "-------                              -----------"
	@echo "make info                            Show the available make commands"
	@echo "make start                           Start the Kind environment"
	@echo "make delete                          Delete the Kind environment"
	@echo "make ingress-nginx                   Setup Ingress NGINX"
	@echo "make argocd                          Setup Argodcd"
	@echo ""

start:
	@echo "=== Starting the Kind environment ==="
	kind create cluster --config kind-config.yaml

delete:
	@echo "=== Deleting the Kind environment ==="
	kind delete cluster

ingress-nginx:
	@echo "=== Deploy NGINX Ingress environment ==="
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

argocd:
	@echo "=== Deploy ArgodCD environment ==="
	kubectl create namespace argocd
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
	kubectl apply -n argocd -f argocd/ingress.yaml
	@echo "Argo admin password: "
	kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
