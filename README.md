Getting Started
=============

I created a `Makefile` to get started. Check the info command to get started.

```bash
make info

Available commands:

Command                              Description
-------                              -----------
make info                            Show the available make commands
make start                           Start the Kind environment
make delete                          Delete the Kind environment
make ingress-nginx                   Setup Ingress NGINX
make argocd                          Setup Argodcd

```

## KIND 

- Manual Create cluster

```bash
kind create cluster --config kind-config.yaml
```

- Manual setup Ingress NGINX

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
```

## ARGOCD

### metrics-server

#### Manual

- Manual: ArgoCD setup metrics server
  - +NEW APP
    - name: metrics-server
    - project: default
    - syncPolicy: Manual
    - destination:
      - namespace: kube-system
      - server: https://kubernetes.default.svc
    - source:
      - repoURL: https://gitlab.com/conclusionxforce/container/kind-argocd-demo.git
      - targetRevision: HEAD
      - path: metrics-server/manifests/release
 
#### Automated
- Automated: ArgoCD setup metrics server
```bash
kubectl apply -f metrics-server/app-metrics-server.yaml
```


### kubernetes-dashboard

#### Manual

- Manual: ArgoCD setup kubernetes-dashboard
  - +NEW APP
    - name: kubernetes-dashboard
    - project: default
    - syncPolicy: Manual
    - destination:
      - namespace: kubernetes-dashboard
      - server: https://kubernetes.default.svc
    - source:
      - repoURL: https://gitlab.com/conclusionxforce/container/kind-argocd-demo.git
      - targetRevision: HEAD
      - path: kubernetes-dashboard/manifests

#### Automated

- Automated: ArgoCD setup kubernetes-dashboard

```bash
kubectl proxy
firefox http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:https/proxy/#/login
```


